This tool is an example dotnet tool for the Toolforge environment using the build
service.

See more details here:
https://wikitech.wikimedia.org/wiki/Help:Toolforge/My_first_Buildpack_.NET_tool
